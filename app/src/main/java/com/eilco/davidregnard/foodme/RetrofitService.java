package com.eilco.davidregnard.foodme;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitService {

    String ENDPOINT = "https://world.openfoodfacts.org";

    @GET("/categories.json")
    Call<CategoryList> getCategoryList();

}