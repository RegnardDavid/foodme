package com.eilco.davidregnard.foodme;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder>{

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.category_name);
        }
    }

    private List<Category> categories;

    public CategoriesAdapter(List<Category> c) {
        categories = c;
    }

    public void setCategories(List<Category> categoryList) {
        this.categories = categoryList;
        notifyDataSetChanged();
    }

    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View categoryView = inflater.inflate(R.layout.item_category, parent, false);

        ViewHolder viewHolder = new ViewHolder(categoryView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CategoriesAdapter.ViewHolder viewHolder, int position) {

        Category category = categories.get(position);

        TextView textView = viewHolder.nameTextView;
        textView.setText(category.getName());
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}