package com.eilco.davidregnard.foodme;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.eilco.davidregnard.foodme.RetrofitService.ENDPOINT;

public class CategoriesActivity extends FoodMeActivity{

    private CategoryList listCategories;
    private TextView categoriesNumber;

    private RecyclerView recyclerCategories;
    private CategoriesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_categories);
        super.onCreate(savedInstanceState);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitService service = retrofit.create(RetrofitService.class);
        listCategories = new CategoryList();
        categoriesNumber = findViewById(R.id.categories_number);

        recyclerCategories = findViewById(R.id.recycler_categories);

        recyclerCategories.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CategoriesAdapter(listCategories.getTags());
        recyclerCategories.setAdapter(adapter);


        System.out.println("first step");

        service.getCategoryList().enqueue(new Callback<CategoryList>() {
            @Override
            public void onResponse(Call<CategoryList> call, Response<CategoryList> response) {
                listCategories = response.body();
                Log.d("TAG","Response = "+listCategories);
                adapter.setCategories(listCategories.getTags());

                categoriesNumber.setText( String.format("%d",listCategories.getCount()));
            }
            @Override
            public void onFailure(Call<CategoryList> call, Throwable t) {
            }
        });
    }
}
